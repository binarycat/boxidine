boxidine: main.c
	gcc --std c99 -o boxidine main.c
README: boxidine README.raw
	./boxidine < README.raw > README
boxidine.txt: boxidine.mdoc
	mandoc < boxidine.mdoc > boxidine.txt
all: boxidine README boxidine.txt
