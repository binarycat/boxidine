#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <wchar.h>
#include <locale.h>
#include <stdbool.h>

#define N(U,R,D,L) (((U)<<6)|((R)<<4)|((D)<<2)|(L))

#define BACKLN 3
#define THISLN 4
#define NEXTLN 5
#define BUFLEN 8000
#define outf stdout

/* may replace this with a single shared buffer for all lines */
static wchar_t buf[3][BUFLEN];
/* has the charachter been escaped? */
static bool esc[3][BUFLEN];
/* store 3 lines so we can know what is above and below */
static wchar_t *line[3] = {0};
static int linelen[3] = {0};
static int linei = 0;

/***********
 * autotile based on the types of surounding chars.
 * 0 = regular/none
 * 1 = light boxdraw
 * 2 = heavy boxdraw
 * 3 = doubleline boxdraw
 * order of arguments:
 *   1
 *  4 2
 *   3 
 ***********/

static char *autotile[256] = {
  [N(1,0,0,0)] ="╵",
  [N(0,1,0,0)] ="╶",
  [N(0,0,1,0)] ="╷",
  [N(0,0,0,1)] ="╴",
  [N(1,1,0,0)] ="└",
  [N(0,1,1,0)] ="┌",
  [N(0,0,1,1)] ="┐",
  [N(1,0,0,1)] ="┘",
  [N(0,1,0,1)] ="─",
  [N(1,0,1,0)] ="│",
  [N(1,1,1,0)] ="├",
  [N(0,1,1,1)] ="┬",
  [N(1,0,1,1)] ="┤",
  [N(1,1,0,1)] ="┴",
  [N(1,1,1,1)] ="┼",
  /* i got tired of ordering them nicely,
   * so the rest are in codepoint order */
  [N(0,2,0,2)] ="━",
  [N(2,0,2,0)] ="┃",
  [N(0,2,1,0)] ="┍",
  [N(0,1,2,0)] ="┎",
  [N(0,2,2,0)] ="┏",
  [N(0,0,1,2)] ="┑",
  [N(0,0,2,1)] ="┒",
  [N(0,0,2,2)] ="┓",
  [N(1,2,0,0)] ="┕",
  [N(2,1,0,0)] ="┖",
  [N(2,2,0,0)] ="┗",
  [N(1,0,0,2)] ="┙",
  [N(2,0,0,1)] ="┚",
  [N(2,0,0,2)] ="┛",
  [N(1,2,1,0)] ="┝",
  [N(2,1,1,0)] ="┞",
  [N(1,1,2,0)] ="┟",
  [N(2,1,2,0)] ="┠",
  [N(2,2,1,0)] ="┡",
  [N(1,2,2,0)] ="┢",
  [N(2,2,2,0)] ="┣",
  [N(1,0,1,2)] ="┥",
  [N(2,0,1,1)] ="┦",
  [N(1,0,2,2)] ="┧",
  [N(2,0,2,1)] ="┨",
  [N(2,0,1,2)] ="┩",
  [N(1,0,2,2)] ="┪",
  [N(2,0,2,2)] ="┫",
  [N(0,1,1,2)] ="┭",
  [N(0,2,1,1)] ="┮",
  [N(0,2,1,2)] ="┯",
  [N(0,2,2,1)] ="┰",
  [N(0,1,2,2)] ="┱",
  [N(0,2,2,1)] ="┲",
  [N(0,2,2,2)] ="┳",
  [N(1,1,0,2)] ="┵",
  [N(1,2,0,1)] ="┶",
  [N(1,2,0,2)] ="┷",
  [N(2,1,0,1)] ="┸",
  [N(2,1,0,2)] ="┹",
  [N(2,2,0,1)] ="┺",
  [N(2,2,0,2)] ="┻",
  [N(1,1,1,2)] ="┽",
  [N(1,2,1,1)] ="┾",
  [N(1,2,1,2)] ="┿",
  [N(2,1,1,1)] ="╀",
  [N(1,1,2,1)] ="╁",
  [N(2,1,2,1)] ="╂",
  [N(2,1,1,2)] ="╃",
  [N(2,2,1,1)] ="╄",
  [N(1,1,2,2)] ="╅",
  [N(1,2,2,1)] ="╆",
  [N(2,2,1,2)] ="╇",
  [N(1,2,2,2)] ="╈",
  [N(2,1,2,2)] ="╉",
  [N(2,2,2,1)] ="╊",
  [N(2,2,2,2)] ="╋",

  [N(0,3,0,3)] ="═",
  [N(3,0,3,0)] ="║",
  [N(0,3,1,0)] ="╒",
  [N(0,1,3,0)] ="╓",
  [N(0,3,3,0)] ="╔",
  [N(0,0,1,3)] ="╕",
  [N(0,0,3,1)] ="╖",
  [N(0,0,3,3)] ="╗",
  [N(1,3,0,0)] ="╘",
  [N(3,1,0,0)] ="╙",
  [N(3,3,0,0)] ="╚",
  [N(1,0,0,3)] ="╛",
  [N(3,0,0,1)] ="╜",
  [N(3,0,0,3)] ="╝",
  [N(1,3,1,0)] ="╞",
  [N(3,1,3,0)] ="╟",
  [N(3,3,3,0)] ="╠",
  [N(1,0,1,3)] ="╡",
  [N(3,0,3,1)] ="╢",
  [N(3,0,3,3)] ="╣",
  [N(0,3,1,3)] ="╤",
  [N(0,1,3,1)] ="╥",
  [N(0,3,3,3)] ="╦",
  [N(1,3,0,3)] ="╧",
  [N(3,1,0,1)] ="╨",
  [N(3,3,0,3)] ="╩",
  [N(1,3,1,3)] ="╪",
  [N(3,1,3,1)] ="╫",
  [N(3,3,3,3)] ="╬",

  [N(0,0,0,2)] ="╸",
  [N(2,0,0,0)] ="╹",
  [N(0,2,0,0)] ="╺",
  [N(0,0,2,0)] ="╻",
};

static int
rdline(void)
{
  int i;
  wint_t r;
  
  for(i=0;i<BUFLEN;i++){
	r = fgetwc(stdin);

	if(r == '\\'){
	  esc[linei][i] = true;
	  r = fgetwc(stdin);
	}
	if(r == WEOF || r == '\n'){
	  goto Endline;
	}

	buf[linei][i] = r;
  }
  fprintf(stderr, "warning: line is too long\n");
 Endline:
  line[linei] = r == WEOF && i == 0 ? NULL : buf[linei];
  linelen[linei++] = i;
  linei %= 3;
  return 0;
}

static wchar_t
linec(int idx, int lnrel)
{
  int l = (linei+lnrel)%3;
  if(0<=idx && idx<linelen[l])
	return line[l][idx];
  return 0;
}

static char
ccat(wchar_t c)
{
  if(c == '=') return 3;
  if(c == '#') return 2;
  if(c == '|') return 1;
  return 0;
}

static void
outc(wchar_t c)
{
  fputwc(c, outf);
  fflush(outf);
}

/* do bitwise magic to replace nonzero neighbors
 * with t */
int
fallback(int t, int m)
{
  int l = m & N(1,1,1,1);
  int h = m & N(2,2,2,2);
  return N(t,t,t,t) & (l|l*2|h|h/2);
}

int
main(int argc, char **argv)
{
  wchar_t c;
  char t;
  int i;
  char *r;

  outf = stdout;
  setlocale(LC_ALL, "");
  /* read the first line and the next line */
  rdline();
  rdline();
  while(line[(linei+THISLN)%3]){
	for(i=0;c=linec(i, THISLN);i++){
	  if((t=ccat(c))&&!esc[(linei+THISLN)%3][i]){
		int m = N(ccat(linec(i, BACKLN)),
				  ccat(linec(i+1, THISLN)),
				  ccat(linec(i, NEXTLN)),
				  ccat(linec(i-1, THISLN)));
		if(t == 1)
		  m = fallback(t, m);
		r = autotile[m];
		if(r == 0)
		  r = autotile[fallback(t, m)];
		/* should only happen if there is no charachter to use */
		if(r == 0) r = "?";
		fflush(outf);
		write(1, r, strlen(r));
	  }else
		outc(c);
	}
	outc('\n');
	fflush(outf);
	rdline();
  }
  return 0;
}
